module Preprocessor where

import Control.Applicative (Alternative)
import Data.Void
import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.Trans
import Text.Printf
import Text.Megaparsec
import Text.Megaparsec.Char


type PT = ParsecT Void String
type SwitchMap = [(String, Integer)]
type DeclareMap = [(String, String)]
type VarData = (SwitchMap, DeclareMap)
newtype Ident = Ident String deriving Show


rTrim :: String -> String
rTrim = reverse . (dropWhile isSpace) .  reverse

-- Like many till, but returns also the final element matched
manyTill2 :: (Monad m, Alternative m) => m a -> m b -> m ([a], b)
manyTill2 p end = go
  where
    go = do
      mx <- optional end
      case mx of
        Just x -> return ([], x)
        Nothing -> do
          (res, x) <- chain p go
          return (res, x)
    chain p go = do
      h <- p
      (t, x) <- go
      return (h:t, x)

-- like manyTill, but stop before the end
manyTill3 :: MonadParsec e s m => m a -> m b -> m [a]
manyTill3 p end = go
  where
    go = do
      mx <- optional (lookAhead end)
      case mx of
        Just _ -> return []
        Nothing -> do
          res <- chain p go
          return res
    chain p go = do
      h <- p
      t <- go
      return (h:t)


-- all spaces and comments, returned string can be empty
pSpaceItem :: PT IO String
pSpaceItem = some spaceChar <|> try pComment

pSpace :: PT IO String
pSpace = concat <$> many pSpaceItem

pSpace1 :: PT IO String
pSpace1 = concat <$> some pSpaceItem


-- a new line is rn or n or r
pNewLine :: PT IO ()
pNewLine = (string "\r\n" <|> string "\n" <|> string "\r") >> return ()

pEOL :: PT IO ()
pEOL = pNewLine <|> eof

pQuoted :: PT IO String
pQuoted = do
  char '\''
  s <- many (noneOf ['\''])
  char '\''
  return ("\'" ++ s ++ "\'")

-- does not accept unfinished comments
pComment :: PT IO String
pComment = do
  o <- getOffset
  string "/*"
  (s1, s2) <- manyTill2 (printChar <|> spaceChar) (string "*/" <|> (string "" <* eof))
  if s2 /= "*/"
    then do
      setOffset o
      fail "unfinished comment"
    else return ("/*" ++ s1 ++ "*/")

pWord :: String -> PT IO ()
pWord word = do
  string' word
  m <- lookAhead (optional alphaNumChar)
  case m of
    Just _ -> fail "nope"
    Nothing -> return ()

mkIdent :: String -> String
mkIdent str = map toLower $ filter (/='$') str

pIdentStr :: PT IO (String, Ident)
pIdentStr = do
  h <- letterChar
  t <- many (alphaNumChar <|> char '$')
  let s = h:t
  return (s, Ident $ mkIdent s)

-- parser a quoted string, returns resolving the quotes
-- remove the first and last quotes, replace '' by '
pStr :: PT IO (String, String)
pStr = do
  s <- many (char '\'' *> many (noneOf "\'") <* char '\'')
  return (concat $ fmap (\x -> printf "'%s'" x) s  , intercalate "'" s)

pInclude :: VarData -> PT IO (String, VarData)
pInclude vData = do
  char '$' >> hspace
  pWord "include" >> hspace
  char '('
  filename <- some (noneOf ")")
  char ')'
  s <- lift $ readFile filename
  hspace >> pEOL
  return (s, vData)

pSetParams :: PT IO SwitchMap
pSetParams = do
  hspace
  (name, val) <- nv
  nvs <- many (hspace *> (char ',') *> nv)
  return ((name, val):nvs)
  where nv = do
          hspace
          (_, Ident name) <- pIdentStr
          mval <- optional (hspace *> (char '=') *> hspace *> (some digitChar))
          let val = read $ fromMaybe "1" mval
          return (name, val)

pSet :: VarData -> PT IO (String, VarData)
pSet (sMap, dMap) = do
  char '$' >> hspace
  pWord "set" >> hspace
  char '('
  setParams <- pSetParams
  let names = fmap fst setParams
  char ')'
  let sMap0 = filter (\(n, _) -> not $ elem n names) sMap
  let sMap1 = concat [setParams, sMap0]
  hspace >> pEOL
  return ("", (sMap1, dMap))


pReset :: VarData -> PT IO (String, VarData)
pReset (sMap, dMap) = do
  char '$' >> hspace
  string' "reset" >> hspace
  char '('
  names <- (fmap . fmap) fst pSetParams
  char ')'
  let sMap1 = filter (\(n, _) -> not $ elem n names) sMap
  hspace >> pEOL
  return ("", (sMap1, dMap))


-- declare <declareItem> (, <declareItem>)* ;
-- declareItem = <ident> literally '<str>'
--             | <ident> <...>
pDeclareItem :: DeclareMap -> PT IO (String, DeclareMap)
pDeclareItem dMap = do
  (s1, Ident identStr) <- pIdentStr
  s2 <- pSpace
  ms3 <- optional (string' "literally")
  case ms3 of
    Just s3 -> do
      s4 <- pSpace
      (s5, str) <- pStr
      s6 <- pSpace
      let dItem = (identStr, str)
      return (concat [s1, s2, s3, s4, s5, s6], dItem : dMap)
    Nothing -> do
      s <- concat <$> many (pSpc <|> (fmap return (noneOf ",;")))
      return (s1 ++ s2 ++ s, dMap)
      where pSpc = concat <$> some (some spaceChar <|> try pComment)

pDeclareItems :: DeclareMap -> PT IO (String, DeclareMap)
pDeclareItems dMap = do
  (s1, m1) <- pDeclareItem dMap
  s1sp <- pSpace
  mtail <- optional (pDeclareItemsTail m1)
  char ';'
  case mtail of
    Just (s2, m2) -> return (s1 ++ s1sp ++ s2 ++ ";", m2)
    Nothing -> return (s1 ++ s1sp ++ ";", m1)

-- zero or many <comma> <declare item>
pDeclareItemsTail :: DeclareMap -> PT IO (String, DeclareMap)
pDeclareItemsTail dMap = do
  char ','
  s1sp1 <- pSpace
  (s1, m1) <- pDeclareItem dMap
  s1sp2 <- pSpace
  mtail <- optional (pDeclareItemsTail m1)
  case mtail of
    Just (s2, m2) -> return ("," ++ s1sp1 ++ s1 ++ s1sp2 ++ s2, m2)
    Nothing -> return ("," ++ s1sp1 ++ s1 ++ s1sp2, m1)

pDeclare :: VarData -> PT IO (String, VarData)
pDeclare (sMap, dMap) = do
  s1 <- string' "declare"
  s2 <- pSpace1
  (s3, dMap1) <- pDeclareItems dMap
  sp <- pSpace
  return (s1 ++ s2 ++ s3 ++ sp, (sMap, dMap1))

pKWItem :: VarData -> PT IO (String, VarData)
pKWItem vData = do
  (s1, m1) <- (try (pInclude vData) <|> try (pSet vData) <|> pReset vData)
  return (s1, m1)

pIdent :: VarData -> PT IO (String, VarData)
pIdent vData@(sMap, dMap)= do
  (str, Ident idStr)  <- pIdentStr
  spc <- pSpace
  case lookup idStr dMap of
    Just s -> return (s ++ spc, vData)
    Nothing -> return (str ++ spc, vData)

pSkipIf :: PT IO ()
pSkipIf = do
  char '$' >> hspace
  pWord "if" >> hspace
  pSkip pEndIf
  pEndIf
  return ()

pControl :: PT IO String
pControl = do
  char '$' >> hspace
  s <- some alphaNumChar
  manyTill printChar pEOL
  return s

pSkip :: PT IO () -> PT IO ()
pSkip pEnd = do
  pSpace
  manyTill3 ((sIf <|> sControl <|> sText) <* pSpace) (try pEnd)
  return ()
  where
    sIf = try pSkipIf
    sControl = do
      mc <- optional (try pControl)
      case mc of
        Just c ->
          if elem (toLower <$> c) ["if", "endif"]
          then fail "bad if syntax"
          else return ()
        Nothing -> fail "..."
    sText = pTextItem >> return ()

pTextItem :: PT IO String
pTextItem = do
  s <- some alphaNumChar <|> pComment <|> pQuoted <|> ((:[]) <$> oneOf "+-*/<>=:;,()@_[]{}")
  return s

pVal :: VarData -> PT IO Integer
pVal (map, _) = do
  mn <- optional pIdentStr
  case mn of
    Just (_, Ident name) -> do
      return $ fromMaybe 0 (lookup name map)
    Nothing -> do
      digits <- some digitChar
      return (read digits)

pIf :: VarData -> PT IO (String, VarData)
pIf vData = do
  char '$' >> hspace
  pWord "if" >> hspace
  val <- pVal vData <* hspace
  mval <- optional $ (char '=') *> hspace *> pVal vData <* hspace
  pNewLine
  let cond = case mval of
        Just val2 -> val == val2
        Nothing -> (mod val 2) == 1
  if cond then do
    result <- pItems (try pElse <|> pEndIf) vData
    mElse <- optional (try pElse)
    case mElse of
      Just _ -> do
        pSkip (try pElse <|> pEndIf)
        pEndIf
        return result
      Nothing -> do
        pEndIf
        return result
  else do
    x <- pSkip (try pElse <|> pEndIf)
    mElse <- optional (try pElse)
    case mElse of
      Just _ -> do
        result <- pItems pEndIf vData
        pEndIf
        return result
      Nothing -> do
        pEndIf
        return ("", vData)

pElse :: PT IO ()
pElse = do
  char '$' >> hspace
  pWord "else" >> hspace >> pNewLine
  return ()

pEndIf :: PT IO ()
pEndIf = do
  char '$' >> hspace
  pWord "endif" >> hspace >> pEOL
  return ()

pItem :: VarData -> PT IO (String, VarData)
pItem vData = (try (pIf vData) <|> pKWItem vData
                <|> pDeclare vData <|> pIdent vData <|> pText)
  where pText = do
          text <- concat <$> some pTextItem
          spc <- pSpace
          return (text ++ spc, vData)

-- parse items until pEnd
pItems :: PT IO () -> VarData -> PT IO (String, VarData)
pItems pEnd vData = do
  sp0 <- pSpace
  mEnd <- optional $ try (lookAhead pEnd)
  case mEnd of
    Just _ -> do
      return ("", vData)
    Nothing -> do
      (s1, vData1) <- pItem vData
      (s2, vData2) <- (pItems pEnd) vData1
      return (sp0 ++ s1 ++ s2, vData2)

pass :: String -> IO String
pass text = do
  e <- runParserT (pItems eof ([], [])) "" text
  case e of
    Left err -> error (show err)
    Right (string, vData) -> do
      return string

passes :: String -> IO String
passes text = do
  text2 <- pass text
  if text == text2
    then return text
    else passes text2

preprocessor :: String -> IO String
preprocessor text = passes text

run :: String -> IO ()
run fn = do
  text <- readFile fn
  code <- passes text
  putStrLn code
  return ()

-- TODO: $ has to be the first character on the line
