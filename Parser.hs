module Parser where

import Data.Void
import Data.Char (toLower)
import Data.Maybe
import Data.Either

import Numeric

import Text.Megaparsec hiding (Label)
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

data Module = Module Identifier DoBlock deriving Show
data BlockBody = BlockBody [Declaration] [BodyItem] (Maybe Identifier) deriving Show
data Declaration
  = Procedure Identifier [Identifier] BlockBody
  | Function Identifier [Identifier] PLMType BlockBody
  | VariableDeclaration Identifier PLMType -- TODO: add initial value
  deriving Show

data DoBlock = DoBlock [String] BlockBody deriving Show
data WhileBlock = WhileBlock Expression BlockBody deriving Show
data IterBlock = IterBlock Identifier Expression Expression BlockBody deriving Show
data CaseBlock = CaseBlock Expression [Statement] deriving Show

data Label = Label Identifier deriving Show
data IfStatement = IfStatement Expression Statement (Maybe Statement) deriving Show
data Assignment = Assignment [(Identifier, [Expression])] Expression deriving Show
data CallStatement = CallStatement Expression [Expression] deriving Show
data ReturnStatement = ReturnStatement Expression deriving Show
data GoStatement = GoStatement Identifier deriving Show
data BodyItem = BodyItem [Label] (Maybe Statement) deriving Show
data Statement
  = StWhile WhileBlock | StIter IterBlock | StCase CaseBlock | StDo DoBlock
  | StAsgn Assignment | StIf IfStatement | StCall CallStatement
  | StRet ReturnStatement | StGo GoStatement | StEmpty deriving Show

data PLMType = TByte | TWord | TInt | TBool | TProc deriving Show

data Identifier = Identifier String deriving Show

reservedWords = ["address", "and", "at", "based", "by", "byte", "call", "case",
                 "data", "declare", "disable", "do",
                 "else", "enable", "end", "eof", "external", "go", "goto",
                 "halt", "if", "initial", "interrupt", "label", "literally",
                 "minus", "mod", "not", "or", "plus", "procedure", "public",
                 "reentrant", "return", "structure", "then", "to", "while", "xor"]

eqIdentString :: String -> String -> Bool
eqIdentString x y = (identStr x) == (identStr y)

identStr :: String -> String
identStr s = toLower <$> filter (/= '$') s

getIdentStr :: Identifier -> String
getIdentStr (Identifier s) = identStr s

idMatch :: Identifier -> String -> Bool
idMatch (Identifier x) s = eqIdentString x s

instance Eq Identifier where
  Identifier x == Identifier y = eqIdentString x y

-- space consumer: spaces ok, delimiters ok, alphanum not ok

sc = do
  mp <- optional alphaNumChar
  case mp of
    Nothing -> do
      spaceConsumer
      return ()
    _ -> fail "no space"

-- Primitives
spaceConsumer :: Parser ()
spaceConsumer = L.space space1 empty (L.skipBlockComment "/*" "*/")

lexeme :: Parser a -> Parser a
lexeme  = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol' sc

delimiter :: String -> Parser String
delimiter = L.symbol spaceConsumer

parens = between (delimiter "(") (delimiter ")")
braces = between (delimiter "{") (delimiter "}")
brackets = between (delimiter "[") (delimiter "]")
semicolon = delimiter ";"
comma = delimiter ","
colon = delimiter ":"
dot = delimiter "."
pPlus = delimiter "+"
pMinus = delimiter "-"
pMult = delimiter "*"
pDiv = delimiter "/"
pMod = symbol "mod"
pOr = symbol "or"
pXor = symbol "xor"
pAnd = symbol "and"
pNot = symbol "not"
pLower = delimiter "<"
pGreater = delimiter ">"
pLEq = delimiter "<="
pGEq = delimiter ">="
pNEq = delimiter "<>"
pEq = delimiter "="
pAssign = delimiter ":="

pInteger :: Parser Integer
pInteger = do
  head <- digitChar
  tail <- many (alphaNumChar <|> char '$')
  let s = [head] ++ filter (\c -> c /= '$') (toLower <$> tail)
  let s0 = take ((length s)-1) s
  case last s of
--    'b' -> f $ readBin s0             -- uncomment after updating base >= 4.16
    'o' -> f $ readOct s0
    'q' -> f $ readOct s0
    'h' -> f $ readHex s0
    'd' -> f $ readDec s0
    _ -> f $ readDec s
  where f x = if length x == 0 then fail "bad number"
              else return (fst (x !! 0))

pIdentifier :: Parser Identifier
pIdentifier = do
  s <- (lexeme $ (:) <$> letterChar <*> many (alphaNumChar <|> char '$'))
  let s1 = identStr s
  if elem s1 reservedWords then
    fail (s1 ++ " is a reserved word")
  else
    return $ (Identifier s)


pModule :: Parser Module
pModule = do
  n <- pIdentifier
  symbol ":"
  b <- pDoBlock
  return $ Module n b

pBlockBody :: Parser BlockBody
pBlockBody = do
  d <- many $ try (pVariableDeclaration <|> pProcedure)
  b <- many $ try pBodyItem
  e <- pEnd
  return $ BlockBody d b e

pType :: Parser PLMType
pType =
  (TByte <$ symbol "byte") <|>
  (TWord <$ symbol "word") <|>
  (TWord <$ symbol "address") <|>
  (TInt <$ symbol "integer") <|>
  (TBool <$ symbol "boolean")

pOneMoreSep :: Parser a -> Parser b -> Parser [a]
pOneMoreSep p sep = do
  first <- p
  rest <- many (sep *> p)
  return (first:rest)

pProcedure :: Parser Declaration
pProcedure = do
  o <- getOffset
  n <- pIdentifier
  symbol ":"
  p <- lexeme $ some alphaNumChar
  if p == "procedure"
    then do
      midents <- optional $ parens (pOneMoreSep pIdentifier comma)
      let params = concat (maybeToList midents)
      mt <- optional pType
      symbol ";"
      b <- pBlockBody
      case mt of
        Just t -> return $ Function n params t b
        Nothing -> return $ Procedure n params b
    else do
      setOffset o
      fail "not a procedure"

pVariableDeclaration :: Parser Declaration
pVariableDeclaration = do
  symbol "declare"
  i <- pIdentifier
  tt <- someTill (lexeme $ many (alphaNumChar <|> char ',' <|> char '$')) (symbol ";")
  let t = case toLower <$> (tt !! 0) of
        "bool" -> Just TBool
        "byte" -> Just TByte
        "address" -> Just TWord
        "word" -> Just TWord
        _ -> Nothing
  case t of
    Just vartype -> return $ VariableDeclaration i vartype
    Nothing -> fail "wrong type"

pWhileBlock :: Parser WhileBlock
pWhileBlock = WhileBlock <$>
  (symbol "do" >> symbol "while" >> pExpr <* symbol ";") <*> pBlockBody

pIterBlock :: Parser IterBlock
pIterBlock = IterBlock
  <$> (symbol "do" >> pIdentifier)
  <*> (symbol "=" >> pExpr)
  <*> (symbol "to" >> pExpr <* symbol ";")
  <*> pBlockBody

pCaseBlock :: Parser CaseBlock
pCaseBlock = CaseBlock
  <$> (symbol "do" >> symbol "case" >> pExpr <* symbol ";")
  <*> (many (try pStatement)) <* pEnd

pDoBlock :: Parser DoBlock
pDoBlock = do
  symbol "do"
  s <- manyTill (lexeme $ many (noneOf ";")) (symbol ";")
  b <- pBlockBody
  return $ DoBlock s b

pEnd :: Parser (Maybe Identifier)
pEnd = do
  x <- symbol "end"
  s <- optional pIdentifier
  symbol ";"
  return s


pStatement :: Parser Statement
pStatement =
      (StWhile <$> try pWhileBlock)
  <|> (StIter <$> try pIterBlock)
  <|> (StCase <$> try pCaseBlock)
  <|> (StDo <$> try pDoBlock)
  <|> (StIf <$> try pIfStatement)
  <|> (StCall <$> try pCall)
  <|> (StRet <$> try pReturn)
  <|> (StGo <$> try pGoStatement)
  <|> (StAsgn <$> pAssignment)
  <|> (symbol ";" >> return StEmpty)

pBodyItem :: Parser BodyItem
pBodyItem = do
  o <- getOffset
  labels <- many $ try (Label <$> (pIdentifier <* symbol ":"))
  mend <- optional $ try (symbol "end")
  case mend of
    Nothing -> do
      stat <- pStatement
      return $ BodyItem labels (Just stat)
    _ -> do
      if (length labels) == 0 then do
        setOffset o
        fail "no labels, no statement"
      else
        return $ BodyItem labels Nothing

-- Statement

pAssignment :: Parser Assignment
pAssignment = Assignment <$> pOneMoreSep x comma <*> y
  where x = (,) <$> pIdentifier <*> (fromMaybe [] <$> optional (parens pParams))
        y = (pEq *> pExpr <* symbol ";")

pIfStatement :: Parser IfStatement
pIfStatement = IfStatement <$> (symbol "if" >> pExpr)
  <*> (symbol "then" >> pStatement)
  <*> (optional (symbol "else" >> pStatement))


----- ##### Call statement parsing BEGIN #####

-- parse (.|p)* p ';'
-- return two values as strings:
-- 1. (.|p)*
-- 2. p
-- where p is '(' s ')', where s contains matching parentheses
-- all parentheses, spaces, and quotes are only valid if not quoted
-- when in quoted state, continue until closing quote
pQSep :: Parser (String, String)
pQSep = do
  return undefined

-- parse matching parentheses
newtype ParenStr = ParenStr String deriving Show
data CallItem = CIString String | CIParenStr ParenStr deriving Show

pMP :: Parser String
pMP = do
  p1 <- char '('
  sq <- (concat . maybeToList) <$> optional pQuoted
  s0 <- pS
  s1 <- concat <$> many pMPh
  p2 <- char ')'
  return $ "(" ++ sq ++ s0 ++ s1 ++ ")"
  where pMPh :: Parser String
        pMPh = do
          x <- pMP
          y <- pS
          return (x ++ y)

pParenStr :: Parser ParenStr
pParenStr = ParenStr <$> pMP

pS :: Parser String
pS = concat <$> many (pQuoted <|> (some (noneOf "();")))

pS2 :: Parser String
pS2 = concat <$> some (pQuoted <|> (some (noneOf "();")))

-- parses a quoted string and returns it as is
pQuoted :: Parser String
pQuoted = do
  char '\''
  s <- many (noneOf "\'")
  char '\''
  return ("'" ++ s ++ "'")

pCallItem = ((CIParenStr <$> pParenStr) <|> (CIString <$> pS2))

pCallItems :: Parser [CallItem]
pCallItems = do
  r <- many pCallItem
  return r

mergeCIs :: [CallItem] -> String
mergeCIs cis = concat $ fmap cistr cis
  where cistr (CIParenStr (ParenStr ps)) = ps
        cistr (CIString str) = str

delLast [x] = []
delLast (h:t) = h : delLast t

-- returns two strings: the procedure string and the parameters string
pCallStr :: Parser (String, Maybe String)
pCallStr = do
  cis <- some pCallItem
  let lastCI = last cis
  let (procStr, mParamStr) = case lastCI of
        CIParenStr (ParenStr str) -> (mergeCIs (delLast cis), Just str)
        _ -> (mergeCIs cis, Nothing)
  return (procStr, mParamStr)

pCall :: Parser CallStatement
pCall = do
  symbol "call"
  (procStr, mParamStr) <- pCallStr
  symbol ";"
  let params = case mParamStr of
                 Just paramStr ->
                   case runParser (parens (pOneMoreSep pExpr comma)) "" paramStr of
                     Left _ -> []
                     Right ps -> ps
                 Nothing -> []
  case runParser pExpr "" procStr of
    Left _ -> error "error in call statement"
    Right procExpr -> return $ CallStatement procExpr params

----- ##### Call statement parsing END #####


pReturn :: Parser ReturnStatement
pReturn = ReturnStatement <$> (symbol "return" >> pExpr) <* symbol ";"

pGoStatement :: Parser GoStatement
pGoStatement = GoStatement
  <$> (symbol "goto" <|> (symbol "go" >> symbol "to")  >> pIdentifier) <* symbol ";"

-- Expression

data Expression
  = Expr LogExpr
  | AExpr EmbeddedAssignment
  deriving Show

data EmbeddedAssignment
  = EmbeddedAssignment Identifier LogExpr
  deriving Show

pExpr :: Parser Expression
pExpr = do
  mea <- optional (try pEmbeddedAssignment)
  case mea of
    Just ea -> return $ AExpr ea
    Nothing -> do
      le <- pLogExpr
      return $ Expr le


pEmbeddedAssignment :: Parser EmbeddedAssignment
pEmbeddedAssignment = EmbeddedAssignment <$>
   pIdentifier <*> (pAssign *> pLogExpr)


-- Logical Expressions

data LogNotOp = LogNot deriving Show
data LogOrOp = OrOp | XorOp deriving Show
data LogAndOp = LogAnd deriving Show
data RelOp = LowerOp | GreaterOp | LEqOp | GEqOp | NEqOp | EqOp deriving Show

data LogExpr
  = ULogExpr LogTerm
  | BLogExpr LogTerm LogOrOp LogExpr
  deriving Show

data LogTerm
  = ULogTerm LogFactor
  | BLogTerm LogFactor LogAndOp LogTerm
  deriving Show

data LogFactor = LogFactor (Maybe LogNotOp) LogUFactor deriving Show

data LogUFactor
  = ULogFactor ArithmExpr
  | BLogFactor ArithmExpr RelOp ArithmExpr
  deriving Show

pNotOp :: Parser LogNotOp
pNotOp = LogNot <$ pNot

pOrOp :: Parser LogOrOp
pOrOp = (OrOp <$ pOr) <|> (XorOp <$ pXor)

pAndOp :: Parser LogAndOp
pAndOp = LogAnd <$ pAnd

pRelOp :: Parser RelOp
pRelOp =
  (NEqOp <$ pNEq)
  <|> (LEqOp <$ pLEq)
  <|> (GEqOp <$ pGEq)
  <|> (LowerOp <$ pLower)
  <|> (GreaterOp <$ pGreater)
  <|> (EqOp <$ pEq)

pLogExpr :: Parser LogExpr
pLogExpr = do
  term <- pLogTerm
  tail <- optional $ do
    op <- pOrOp
    ex <- pLogExpr
    return (op, ex)
  case tail of
    Just (op, ex) -> return $ BLogExpr term op ex
    Nothing -> return $ ULogExpr term

pLogTerm :: Parser LogTerm
pLogTerm = do
  factor <- pLogFactor
  tail <- optional $ do
    op <- pAndOp
    term <- pLogTerm
    return (op, term)
  case tail of
    Just (op, term) -> return $ BLogTerm factor op term
    Nothing -> return $ ULogTerm factor

pLogFactor :: Parser LogFactor
pLogFactor = do
  sign <- optional pNotOp
  ae <- pArithmExpr
  op <- optional pRelOp
  uf <- case op of
          Just relOp -> do
            ae2 <- pArithmExpr
            return $ BLogFactor ae relOp ae2
          Nothing -> return $ ULogFactor ae
  return $ LogFactor sign uf


-- Arithmetic Expressions

data SignOp = SPlus | SMinus deriving Show
data AddOp = APlus | AMinus deriving Show
data MulOp = MulOp | ModOp | DivOp deriving Show

data ArithmExpr
  = UArithmExpr ArithmTerm
  | BArithmExpr ArithmTerm AddOp ArithmExpr
  deriving Show

data ArithmTerm
  = UArithmTerm ArithmFactor
  | BArithmTerm ArithmFactor MulOp ArithmTerm
  deriving Show

data ArithmUFactor
  = PConst Integer
  | PVar Identifier [Expression]
  | PExpr Expression
  deriving Show

data ArithmFactor
  = ArithmFactor (Maybe SignOp) ArithmUFactor
  deriving Show

pSignOp :: Parser SignOp
pSignOp = (SPlus <$ pPlus) <|> (SMinus <$ pMinus)

pAddOp :: Parser AddOp
pAddOp = (APlus <$ pPlus) <|> (AMinus <$ pMinus)

pMulOp :: Parser MulOp
pMulOp = (MulOp <$ pMult) <|> (ModOp <$ pMod) <|> (DivOp <$ pDiv)

pParams :: Parser [Expression]
pParams = do
  e <- pExpr
  more <- optional $ comma
  case more of
    Nothing -> return [e]
    Just _ -> do
      t <- pParams
      return (e:t)

pArithmExpr :: Parser ArithmExpr
pArithmExpr = do
  term <- pArithmTerm
  tail <- optional $ do
    op <- pAddOp
    ex <- pArithmExpr
    return (op, ex)
  case tail of
    Just (op, ex) -> return $ BArithmExpr term op ex
    Nothing -> return $ UArithmExpr term

pArithmTerm :: Parser ArithmTerm
pArithmTerm = do
  factor <- pArithmFactor
  tail <- optional $ do
    op <- pMulOp
    term <- pArithmTerm
    return (op, term)
  case tail of
    Just (op, term) -> return $ BArithmTerm factor op term
    Nothing -> return $ UArithmTerm factor

pArithmFactor :: Parser ArithmFactor
pArithmFactor = do
  sign <- optional pSignOp
  uf <- (PConst <$> pInteger) <* sc
    <|> (PVar <$> pIdentifier <*> ((concat . maybeToList) <$> optional (parens pParams)))
    <|> (PExpr <$> parens pExpr)
  return $ ArithmFactor sign uf

parseString function string = runParser (spaceConsumer >> function) "" string

parseFile filename = do
  contents <- readFile filename
  return $ parseString pModule contents
