#!/usr/bin/env python3

import plmhs
import plmz80
from pathlib import Path

def run_test(program, args):
    progname = str(Path(program).stem)
    reshs = plmhs.plm_run(program, args).stdout.decode("utf-8").strip()
    resz80 = plmz80.plm_run(program, args).stdout.decode("utf-8").strip()
    if reshs == resz80:
        print("success: {}({}) = {}"
              .format(progname, ",".join(map(str,args)), reshs))
    else:
        print("failure")

# fib(10) = 55
run_test("tests/fib.plm", [10])

# gcd(15, 25) = 5
run_test("tests/gcd.plm", [15, 25])
