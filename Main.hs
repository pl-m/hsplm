module Main where

import System.Environment
import Data.List.Split
import Text.Read

import Preprocessor
import Eval

parseArg :: String -> Maybe (Integer, PLMVal)
parseArg arg =
  case splitOn ":" arg of
    [sp, sv] ->
      let mp = readMaybe sp :: Maybe Integer
          mv = str2val sv
      in case (mp, mv) of
        (Just p, Just v) -> Just (p, v)
        _ -> Nothing
    [sv] ->
      case str2val sv of
        (Just v) -> Just (1, v)
        _ -> Nothing
    _ -> Nothing

printOutput :: PLMState -> IO ()
printOutput (PLMState _ _ outList) = loop outList
    where loop [] = return ()
          loop ((PortVal (a,b)):t) = do
            if a == 1
              then putStrLn $ (show b)
              else putStrLn $ (show a) ++ ":" ++ (show b)
            loop t

getOutput :: PLMState -> [(Integer, Integer)]
getOutput (PLMState _ _ outList) = fmap (\(PortVal (x, y)) -> (x, integ y)) outList

{-
parseString function string = runParser (spaceConsumer >> function) "" string
parseFile filename = do
  contents <- readFile filename
  return $ parseString pModule contents
-}

main :: IO ()
main = do
  (file:args) <- getArgs
  let minput = sequenceA (parseArg <$> args)
  case minput of
    Just input -> do
      contents0 <- readFile file
      contents1 <- preprocessor contents0
      let state = runModule contents1 (PortVal <$> input)
      printOutput state
    Nothing -> putStrLn "bad input"

runFile :: String -> String -> IO [(Integer, Integer)]
runFile file args = do
  let minput = sequenceA (parseArg <$> (splitOn " " args))
  case minput of
    Just input -> do
      contents0 <- readFile file
      contents1 <- preprocessor contents0
      let state = runModule contents1 (PortVal <$> input)
      return $ getOutput state
    Nothing -> fail "bad input"


{-

$ bin/hsplm tests/ander.plm 7 21
5
$ bin/hsplm tests/fib.plm 10
55
$ bin/hsplm tests/gcd.plm 15 25
5
$ bin/hsplm tests/shift.plm 13 3
104
161
104
1

λ> runFile "tests/ander.plm" "7 21"
[(1,5)]
λ> runFile "tests/fib.plm" "10"
[(1,55)]
λ> runFile "tests/gcd.plm" "15 25"
[(1,5)]
λ> runFile "tests/shift.plm" "13 3"
[(1,1),(1,104),(1,161),(1,104),(1,1),(1,104),(1,40961),(1,104)]

-}
