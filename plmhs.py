#!/usr/bin/env python3

import os
import sys
import subprocess

hsplm = "./bin/hsplm"

if not os.path.isfile(hsplm):
    print("File \"{}\" does not exist".format(hsplm), file=sys.stderr)
    print("Goodbye, World!", file=sys.stderr)
    exit(-1)

def plm_run(filename, args):
    params = []
    for arg in args:
        if type(arg) is tuple:
            params.append("{}:{}".format(arg[0], arg[1]))
        else:
            params.append(str(arg))
    return subprocess.run([hsplm, filename] + params, capture_output = True)


#print(plm_run("tests/fib.plm", [(1,10)]).stdout)
#print(plm_run("tests/gcd.plm", [15, 25]).stdout)
