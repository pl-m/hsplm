#!/usr/bin/env bash

calldir=$(pwd)

if [[ $# == 1 ]]
then
    if [[ ! -f $1 ]]
    then
        echo "File \"$1\" does not exist."
        exit -2
    fi
    path=$1
    directory="$(dirname $path)"
    filename="$(basename $path)"
    name="${filename%.*}"
    extension="${path##*.}"
else
    echo 'usage: z80.sh <source file>'
    exit -1
fi

cd "$directory"
tools_dir=~/prog/tools/z80
zxcc $tools_dir/zsm4 $name,$name=$name.z80
zxcc $tools_dir/drlink $name
mv $name.com $name.bin
rm "$name".{prn,rel,sym}
if [[ ! "$calldir" -ef "$directory" ]]
then
    mv "$name.bin" "$calldir"
fi
