#!/usr/bin/env bash

if [[ $# == 1 ]]
then
    if [[ ! -f $1 ]]
    then
        echo "File \"$1\" does not exist."
        exit -2
    fi
    path=$1
    directory="$(dirname $path)"
    filename="$(basename $path)"
    name="${filename%.*}"
    extension="${path##*.}"
else
    echo 'usage: plm.sh <source file>'
    exit -1
fi

export ISIS_F0="$directory"
export ISIS_F1=~/prog/tools/plm
thames :f1:plm80 $name.plm
thames :f1:link $name.obj to $name.mod
thames :f1:locate $name.mod "code(00H)" "stacksize(100)"
mv "$directory/$name" "$directory/$name.dat"
thames :f1:objhex $name.dat to $name.hex
objcopy --input-target=ihex --output-target=binary "$directory/$name.hex" "$directory/$name.bin"
rm "$directory/$name".{obj,mod,dat,hex,lst}
if [[ ! . -ef "$directory" ]]
then
    mv "$directory/$name.bin" .
fi
