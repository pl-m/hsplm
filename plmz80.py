#!/usr/bin/env python3

import os
import sys
import subprocess
from pathlib import Path

# define paths
bin = Path("bin")

thames = bin.joinpath("thames")
z80 = bin.joinpath("z80")

isis = bin.joinpath("isis")

# define environment variables
os.environ["ISIS_F0"] = str(isis)
os.environ["ISIS_F1"] = str(isis.joinpath("plm80"))
os.environ["ISIS_F2"] = str(isis.joinpath("asm80"))
os.environ["ISIS_F3"] = str(isis.joinpath("utils"))

def build(filestr):
    fullpath = Path(filestr).resolve()
    dirpath = fullpath.parent  # the directory where the file is located
    fullname = fullpath.name   # the name of the file with extension
    name = fullpath.stem       # file name without path and extension
    extension = fullpath.suffix

    if not fullpath.exists():
        print("File \"{}\" does not exist".format(str(fullpath)),
              file=sys.stderr)
        exit(-1)

    if (extension != ".plm"):
        print("bad file extension: \"{}\"".format(extension), file=sys.stderr)
        exit(-1)

    os.environ["ISIS_F4"] = str(dirpath)

    hexfile = dirpath.joinpath(name).with_suffix(".hex")
    binfile = dirpath.joinpath(name).with_suffix(".bin")
    objfile = dirpath.joinpath(name).with_suffix(".obj")
    tmpfile = dirpath.joinpath(name).with_suffix(".tmp")
    modfile = dirpath.joinpath(name).with_suffix(".mod")
    lstfile = dirpath.joinpath(name).with_suffix(".lst")

    # obj from plm
    params = ":f1:plm80 :f4:{}.plm".format(name)
    subprocess.run([thames] + params.split(), capture_output=True)

    # mod from obj
    params = ":f3:link :f4:{}.obj,:f1:x0100 to :f4:{}.mod".format(name, name)
    subprocess.run([thames] + params.split(), capture_output=True)

    # tmp from mod
    params = ":f3:locate :f4:{}.mod code(0000H) stacksize(100)".format(name)
    subprocess.run([thames] + params.split(), capture_output=True)
    # rename ander ander.tmp
    subprocess.call(["mv", dirpath.joinpath(name), "{}".format(tmpfile)])

    # hex from tmp
    params = ":f3:objhex :f4:{}.tmp to :f4:{}.hex".format(name, name)
    subprocess.call([thames] + params.split())

    # bin from hex
    subprocess.call(["objcopy", "--input-target=ihex",
                     "--output-target=binary",
                     "{}".format(hexfile), "{}".format(binfile)])

    # delete temporary files: obj, tmp, mod, hex, lst
    subprocess.call(["rm", "{}".format(objfile),
                     "{}".format(tmpfile), "{}".format(modfile),
                     "{}".format(hexfile), "{}".format(lstfile)])

    # done
    result = dirpath.joinpath(name).with_suffix(".bin")
    if result.exists():
        return str(result)
    else:
        print("Error building {}".format(filestr), file=sys.stderr)
        exit(-1)

def plm_run(filestr, args):
    params = []
    for arg in args:
        if type(arg) is tuple:
            params.append("{}:{}".format(arg[0], arg[1]))
        else:
            params.append(str(arg))
    binfile = build(filestr)
    return subprocess.run([z80, binfile] + params, capture_output = True)

#print(plm_run("tests/fib.plm", [(1,10)]).stdout)
#print(plm_run("tests/gcd.plm", [15, 25]).stdout)
