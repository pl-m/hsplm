module PreprocessorTest where

import Data.Maybe
import Data.Either

import Preprocessor

import Test.Hspec

import Text.Megaparsec
import Text.Megaparsec.Char

data Test = Test {
  p :: VarData -> PT IO (String, VarData),
  vData :: VarData,
  str ::  String,
  out :: (String, VarData)
}


test (Test p vData s out) = do
  shouldReturn
    (runParserT (p vData) "" s)
    (pure out)

tst p s = fromRight undefined $ runParser p "" s

testInclude :: SpecWith ()
testInclude = do
  describe "include a file" $ do
    it "include test.inc" $
      test $ Test { p = pInclude,
                    vData = ([], []),
                    str = "$include(tests/test.inc)",
                    out = ("<test>", ([], []))
                  }
    it "include test.inc with newline" $
      test $ Test { p = pInclude,
                    vData = ([], []),
                    str = "$include(tests/test.inc)\n",
                    out = ("<test>", ([], []))
                  }

testSet :: SpecWith ()
testSet =  do
  describe "set a variable" $ do
    it "set a default value" $
      test $ Test { p = pSet,
                     vData = ([],[]),
                     str = "$set (r)",
                     out = ("",([("r",1)],[]))
                   }
    it "set a specific value" $
      test $ Test { p = pSet,
                    vData = ([],[]),
                    str = "$set (xx=2)",
                    out = ("",([("xx",2)],[]))
                  }

testReset :: SpecWith ()
testReset = do
  describe "reset control" $ do
    it "remove a value" $
      test $ Test { p = pReset,
                    vData = ([("a",10),("b",20)], []),
                    str = "$reset (a)",
                    out = ("", ([("b",20)], []))
                  }
    it "remove no value" $
      test $ Test { p = pReset,
                    vData = ([("a",10),("b",20)], []),
                    str = "$reset (c) \r\n",
                    out = ("", ([("a", 10),("b",20)], []))
                  }

testIf :: SpecWith ()
testIf = do
  describe "simple test if" $ do
    it "if false" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 0\n2 \n$else\n3 \n$endif",
                     out = ("3 \n",([],[]))
                   }
    it "if true" $
      test $ Test { p = pIf,
                     vData = ([("a",0)], []),
                     str = "$if a=0 \n 2 \n$else\n 3 \n$endif",
                     out = (" 2 \n",([("a",0)],[]))
                   }
    it "if true without else" $
      test $ Test { p = pIf,
                     vData = ([("a",1)], []),
                     str = "$if a \r\n x \n$endif",
                     out = (" x \n", ([("a",1)], []))
                   }
    it "if false without else" $
      test $ Test { p = pIf,
                     vData = ([("a",1)], []),
                     str = "$if 0 \rx \n$endif",
                     out = ("", ([("a",1)], []))
                   }
    it "if test true without else" $
      test $ Test { p = pIf,
                     vData = ([("a",1)], []),
                     str = "$if a=a \rx=9; \n$endif",
                     out = ("x=9; \n",([("a",1)],[]))
                   }
    it "if test false without else" $
      test $ Test { p = pIf,
                     vData = ([("a",1)], []),
                     str = "$if a=2 \rx=9; \n$endif",
                     out = ("", ([("a",1)], []))
                   }
    it "if test true with else" $
      test $ Test { p = pIf,
                     vData = ([("a",1)], []),
                     str = "$if a=a \n x=9; \n$else \n y=10; \n$endif",
                     out = (" x=9; \n",([("a",1)],[]))
                   }
    it "if test false with else" $
      test $ Test { p = pIf,
                     vData = ([("a",1)], []),
                     str = "$if a=0 \n x=9; \n$else \n y=10; \n$endif",
                     out = (" y=10; \n",([("a",1)],[]))
                   }

testDeclare :: SpecWith ()
testDeclare =  do
  describe "simple declare test" $ do
    it "empty declare" $
      test $ Test { p = pDeclare,
                     vData = ([], []),
                     str = "declare x, y byte;",
                     out = ("declare x, y byte;",([],[]))
                   }
    it "declare one item" $
      test $ Test { p = pDeclare,
                     vData = ([], []),
                     str = "declare x literally 'abc';",
                     out = ("declare x literally 'abc';",([],[("x", "abc")]))
                   }
    it "declare two items" $
      test $ Test { p = pDeclare,
                     vData = ([], []),
                     str = "declare x literally 'abc', y literally '1234';",
                     out = ("declare x literally 'abc', y literally '1234';",
                            ([],[("y", "1234"), ("x", "abc")]))
                   }
    it "declare three items" $
      test $ Test { p = pDeclare,
                     vData = ([], []),
                     str = "declare u byte, x literally 'abc', "
                           ++ "y literally '1234';",
                     out = ("declare u byte, x literally 'abc', "
                           ++ "y literally '1234';",
                            ([],[("y", "1234"), ("x", "abc")]))
                   }


testIfElseDeclare :: SpecWith ()
testIfElseDeclare =  do
  describe "simple if declare test" $ do
    it "if true delcare without else" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 1\ndeclare a literally 'lit';\n$endif",
                     out = ("declare a literally 'lit';\n",([],[("a", "lit")]))
                   }
    it "if false delcare without else" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 2\ndeclare a literally 'lit';\n$endif",
                     out = ("",([],[]))
                   }
    it "if true delcare with else" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 1 \ndeclare a literally 'lit1';\n"
                           ++ "$else\n declare a literally 'lit2';\n$endif",
                     out = ("declare a literally 'lit1';\n",([],[("a", "lit1")]))
                   }
    it "if false delcare with else" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 10 \ndeclare a literally 'lit1';\n"
                           ++ "$else\ndeclare a literally 'lit2';\n$endif",
                     out = ("declare a literally 'lit2';\n",([],[("a", "lit2")]))
                   }


testIfElseSet :: SpecWith ()
testIfElseSet =  do
  describe "simple if set/reset test" $ do
    it "if true set without else" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 1\n$set (a)\n$endif",
                     out = ("", ([("a", 1)], []))
                   }
    it "if false set without else" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 2\n$set (a)\n$endif",
                     out = ("",([],[]))
                   }
    it "if true set/reset with else" $
      test $ Test { p = pIf,
                     vData = ([("a", 7)], []),
                     str = "$if 1\n$set (a)\n$else\n$reset (a)\n$endif",
                     out = ("",([("a", 1)], []))
                   }
    it "if false set/reset with else" $
      test $ Test { p = pIf,
                     vData = ([("a", 7)], []),
                     str = "$if 0\n$set (a)\n$else\n$reset (a)\n$endif",
                     out = ("",([], []))
                   }

testMultiple :: SpecWith ()
testMultiple = do
  describe "test multiple items" $ do
    it "set and declare" $
      test $ Test { p = pItems eof,
                     vData = ([],[]),
                     str = "$set (r) \ndeclare a literally 'A', x byte;",
                     out = ("declare a literally 'A', x byte;",
                            ([("r",1)],[("a", "A")]))
                   }
    it "set / declare / reset" $
      test $ Test { p = pItems eof,
                     vData = ([],[]),
                     str = "$set (r) \ndeclare a literally 'A', x byte;"
                           ++ "$reset (r)",
                     out = ("declare a literally 'A', x byte;",
                            ([],[("a", "A")]))
                   }
    it "include and set" $
      test $ Test { p = pItems eof,
                     vData = ([],[]),
                     str = "$include (tests/test.inc) \n$set (r)",
                     out = ("<test>",([("r",1)],[]))
                   }
    it "set + set" $
      test $ Test { p = pItems eof,
                     vData = ([],[]),
                     str = "$set (r) \r\n$set (y=21,a)\r\n",
                     out = ("",([("y", 21), ("a", 1), ("r", 1)],[]))
                   }

testMultipleIf :: SpecWith ()
testMultipleIf = do
  describe "test if/else/endif with multiple items inside" $ do
    it "if true with set and declare, without else" $
      test $ Test { p = pIf,
                     vData = ([],[]),
                     str = "$if 1 \n$set (r) \ndeclare a literally 'A', x byte;\n"
                           ++ "$endif",
                     out = ("declare a literally 'A', x byte;\n",
                            ([("r",1)],[("a", "A")]))
                   }
    it "if true with set, declare, and include, without else" $
      test $ Test { p = pIf,
                     vData = ([],[]),
                     str = "$if 1 \n$set (r) \ndeclare a literally 'A', x byte;\n"
                           ++ "$include (tests/test.inc)\n$endif",
                     out = ("declare a literally 'A', x byte;\n<test>",
                            ([("r",1)],[("a", "A")]))
                   }
    it "if false with set and declare, without else" $
      test $ Test { p = pIf,
                     vData = ([],[("a123", "456")]),
                     str = "$if 0 \n$set (r) \ndeclare a literally 'A', x byte;\n"
                           ++ "$endif",
                     out = ("",([],[("a123", "456")]))
                   }
    it "if true with set and include, with else" $
      test $ Test { p = pIf,
                     vData = ([],[]),
                     str = "$if 1 \n$set (q) \n$include (tests/test.inc)\n"
                           ++ "$else \nx = x + 4*f(y); \n$endif",
                     out = ("<test>",([("q",1)],[]))
                   }
    it "if false with declare and include, with else" $
      test $ Test { p = pIf,
                     vData = ([],[]),
                     str = "$if 0 \n$set (q) \n$else \ndeclare x literally '#';\n"
                           ++ "$include (tests/test.inc)\n$endif",
                     out = ("declare x literally '#';\n<test>",([],[("x", "#")]))
                   }

testItemsIf :: SpecWith ()
testItemsIf = do
  describe "multiple items with if/else/endif" $ do
    it "if false" $
      test $ Test { p = pItems eof,
                    vData = ([], []),
                    str = "call f; \n$if 0 \n2 \n$else \n3 \n$endif\n ;",
                    out = ("call f; \n3 \n ;",([],[]))
                   }
    it "if true" $
      test $ Test { p = pItems eof,
                     vData = ([("a",0)], []),
                     str = "x = 3; /* x must be 3 */ \n$if a=0 \nx=2; "
                           ++ "\n$else\n 3 \n$endif\n call f;",
                     out = ("x = 3; /* x must be 3 */ \nx=2; \n call f;",
                            ([("a",0)],[]))
                   }
    it "if true without else" $
      test $ Test { p = pItems eof,
                     vData = ([("a",3)], []),
                     str = "$set (q)\n$if a \nx \n$endif \n$reset (a)",
                     out = ("x \n", ([("q",1)], []))
                   }
    it "if false without else" $
      test $ Test { p = pItems eof,
                     vData = ([("a",1)], []),
                     str = "$reset(x,y,z) \n$if 0 \nx \n$endif\n"
                           ++ "$include (tests/test.inc)\n",
                     out = ("<test>", ([("a",1)], []))
                   }
    it "if test true without else" $
      test $ Test { p = pItems eof,
                     vData = ([("a",1)], []),
                     str = "call r(x,y); \n$if a=a \nx=9; \n$endif",
                     out = ("call r(x,y); \nx=9; \n",([("a",1)],[]))
                   }
    it "if test false without else" $
      test $ Test { p = pItems eof,
                     vData = ([("a",1)], []),
                     str = "$if a=2 \nx=9; \n$endif",
                     out = ("", ([("a",1)], []))
                   }
    it "if test true with else" $
      test $ Test { p = pItems eof,
                    vData = ([("a",1)], []),
                    str = "$if a=a \nx=9; \n$else\n y=10; \n$endif \n$reset (a,t)",
                    out = ("x=9; \n",([],[]))
                   }
    it "if test false with else" $
      test $ Test { p = pItems eof,
                     vData = ([("a",1)], []),
                     str = "y=9; \n$if a=0 \nx=9; \n$else \ny=10; \n$endif\n y=11;",
                     out = ("y=9; \ny=10; \n y=11;",([("a",1)],[]))
                   }

testNestedIf :: SpecWith ()
testNestedIf =
  describe "test nested if" $ do
    it "T( T(X) )" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 1 \n$if 3 \nx = 2; \n$endif \n $endif",
                     out = ("x = 2; \n",([],[]))
                   }
    it "F( F( | ) )" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 0 \n$if\n 2 \nx = 2; \n$else\n x = 3; $endif \n $endif",
                     out = ("",([],[]))
                   }
    it "T( T(X) | F( ) )" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 1 \n$if 1 \nx = 2; \n$endif\n"
                           ++ "$else \n$if 4 \ny = x; \n$endif \n $endif",
                     out = ("x = 2; \n",([],[]))
                   }
    it "F( T( ) | T(X) )" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 0\n $if 1\n x = 2; \n$endif\n"
                           ++ "$else\n$if 5\n y = x; \n$endif\n $endif",
                     out = (" y = x; \n",([],[]))
                   }
    it "T( F( |X) | F( ) )" $
      test $ Test { p = pIf,
                     vData = ([("a", 1), ("b", 0), ("c", 0)], []),
                     str = "$if 1 \n$if b\n $reset (a) \n$else\n$reset(c)\n$endif\n"
                           ++ "$else \n$if c\n $reset (b)\n$endif\n$endif",
                     out = ("",([("a", 1), ("b", 0)],[]))
                   }
    it "F( T( ) | F( |X) )" $
      test $ Test { p = pIf,
                     vData = ([("a", 1), ("b", 0), ("c", 0)], []),
                     str = "$if b\n$if a\n$reset (a)\n$endif\n"
                           ++ "\n$else\n$if c\n$reset (b)\n$else\n$set(c)\n$endif\n$endif\n",
                     out = ("",([("c", 1), ("a", 1), ("b", 0)],[]))
                   }
    it "T( T(X| ) | F( ) )" $
      test $ Test { p = pIf,
                     vData = ([("a", 1), ("b", 1), ("c", 0)], []),
                     str = "$if 1\n$if b\n$reset (a)\n$else\n$reset (c)\n$endif\n"
                           ++ "$else\n$if c\n$reset (b)\n$endif\n$endif",
                     out = ("",([("b", 1), ("c", 0)],[]))
                   }
    it "F( T( ) | F( |X) )" $
      test $ Test { p = pIf,
                     vData = ([("a", 0), ("b", 1), ("c", 0)], []),
                     str = "$if a\n$if b\n$reset (c)\n$endif\n"
                           ++ "$else\n$if c\n$reset (a)\n$else\n$reset(b)\n$endif\n$endif",
                     out = ("",([("a", 0), ("c", 0)],[]))
                   }
    it "F( T( | ) | T(X| ) )" $
      test $ Test { p = pIf,
                     vData = ([("a", 0), ("b", 1), ("c", 1)], []),
                     str = "$if a\n$if b\n$reset (c)\n$else\n$set (d)\n$endif\n"
                           ++ "$else\n$if c\n$reset (a)\n$else\n$reset(b)\n$endif\n$endif",
                     out = ("",([("b", 1), ("c", 1)],[]))
                   }
    it "T( F( |T(X)) | F( | ) )" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 1\n$if 0\n$set (a)\n$else\n$if 1\n$set (b)\n$endif\n$endif\n"
                           ++ "$else\n$if 0\n$set (c)\n$else\n$set (d)\n$endif\n$endif",
                     out = ("",([("b", 1)],[]))
                   }
    it "F( T(F( )) | T(F( |X)| ) )" $
      test $ Test { p = pIf,
                     vData = ([], []),
                     str = "$if 0\n$if 1\n$if 0\n$set (a)\n$endif\n$endif\n"
                           ++ "$else\n$if 1\n$if 0\n$set (b)\n$else\n$set (c)\n$endif\n"
                           ++ "$else\n$set (d)\n$endif\n$endif\n",
                     out = ("",([("c", 1)],[]))
                   }

testSetDeclare :: SpecWith ()
testSetDeclare =
  describe "test priority between set and declare" $ do
    it "set variable used in if" $
      test $ Test { p = pItems eof,
                     vData = ([], []),
                     str = "$set (a=1) \ndeclare a literally '0'; \n$if a \n$set (a=2) \n$endif",
                     out = ("declare a literally '0'; \n",([("a", 2)],[("a", "0")]))
                   }
    it "use pass" $
      let str = "$set (a=1) \ndeclare a literally '0'; \n$if a \n$set (a=2) \n$endif"
          out = "declare a literally '0'; \n"
      in (pass str) `shouldReturn` out

testPass :: SpecWith ()
testPass =
  describe "test single pass" $ do
    it "test single pass 1" $
      let str = "declare dcl literally 'declare';\ndcl x byte, true literally '1';"
          out = "declare dcl literally 'declare';\ndeclare x byte, true literally '1';"
      in (pass str) `shouldReturn` out
    it "test single pass 2" $
      let str = "$if 1 \n$set (r) \ndeclare a literally '12', x byte;\nx = a;\n$endif"
          out = "declare a literally '12', x byte;\nx = 12;\n"
      in (pass str) `shouldReturn` out

testPasses :: SpecWith ()
testPasses =
  describe "test multiple passes" $ do
    it "test multiple 1" $
      let str = "declare dcl literally 'declare';\ndcl x byte, true literally '1';\nx = true;"
          out = "declare dcl literally 'declare';\ndeclare x byte, true literally '1';\nx = 1;"
      in (passes str) `shouldReturn` out
    it "test multiple 2" $
      let str = "$include (tests/dcl.inc)\n    x = CR;\n    y = LF;\n"
          out = "DECLARE\n    TRUE LITERALLY '1',\n    FALSE LITERALLY '0',\n"
            ++ "    FOREVER LITERALLY 'WHILE TRUE',\n    CTRL$Y LITERALLY '19h',\n"
            ++ "    CR LITERALLY '13',\n    LF LITERALLY '10',\n    WHAT LITERALLY '63';\n"
            ++ "    x = 13;\n"
            ++ "    y = 10;\n"
      in (passes str) `shouldReturn` out

main = hspec $ do
  testInclude
  testSet
  testReset
  testIf
  testDeclare
  testIfElseDeclare
  testIfElseSet
  testMultiple
  testMultipleIf
  testItemsIf
  testNestedIf
  testSetDeclare
  testPass
  testPasses
