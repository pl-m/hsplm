#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "ytypes.h"
#include "mem_mmu.h"

BYTE ram[MEMSIZE*1024];

WORD af[2];
int af_sel;

struct ddregs {
	WORD bc;
	WORD de;
	WORD hl;
} regs[2];

int regs_sel;

WORD ir;
WORD ix;
WORD iy;
WORD sp;
WORD pc;
WORD IFF;

FASTWORK simz80(FASTREG PC);

struct input_item {
    int port;
    int value;
    bool done;
};

int input_size = 0;
struct input_item* input = 0;

int in(unsigned int port) {
    int value;
    int found = false;
    for (int i = 0; i < input_size; ++i) {
        if (port == input[i].port && !input[i].done) {
            value = input[i].value;
            input[i].done = true;
            found = true;
            break;
        }
    }
    if (!found) {
        char buff[10];
        printf("Enter value for port %d: ", port);
        if (fgets (buff, sizeof(buff), stdin) == NULL) {
            fprintf(stderr, "Input Error");
            exit(-1);
        }
        value = atoi(buff);
    }
    return value;
}

void out(unsigned int port, unsigned char value) {
    if (port == 1) {
        printf("%d\n", value);
    } else {
        printf("%d:%d\n", port, value);
    }
}

int main(int argc, char** argv) {

    /* check arguments */
    if (argc < 2) {
        fprintf(stderr, "bad arguments\n");
        return -1;
    }

    /* open file */
    char* fn = argv[1];
    if (access(fn, F_OK) != 0) {
        fprintf(stderr, "File does not exist\n");
        return -1;
    }
    FILE* f = fopen(fn, "r");
    if (f == NULL) {
        fprintf(stderr, "Could not open file");
    }
    fseek(f, 0, SEEK_END);
    long sz = ftell(f);
    fseek(f, 0, SEEK_SET);
    fread(ram, sz, 1, f);
    fclose(f);

    /* read input */
    input_size = argc - 2;
    input = malloc(input_size * sizeof (struct input_item));
    for (int i = 0; i < input_size; ++i) {
        char *s = strtok(argv[i+2], ":");
        char *t = strtok(NULL, ":");
        int port = t == NULL ? 1 : atoi(s);
        int value = t == NULL ? atoi(s) : atoi(t);
        input[i].port = port;
        input[i].value = value;
        input[i].done = false;
    }

    /* run program */
    ram[sz] = 0x76;   /* halt */
    simz80(0);
    free(input);
    return 0;
}
