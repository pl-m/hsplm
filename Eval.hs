module Eval where

import Data.Maybe
import Data.Bits
import Data.Word
import Data.Int
import Text.Read
import Debug.Trace
import Parser

-- type declarations

-- state, input, output
newtype VarMap = VarMap [(Identifier, PLMVal)] deriving Show

newtype PortVal = PortVal (Integer, PLMVal) deriving Show

-- data PLMState = PLMState [(Identifier, PLMVal)] [(Integer, PLMVal)] [(Integer, PLMVal)]
--   deriving Show

data RTLevel = RTLevel {
  params :: VarMap,
  vars :: VarMap
} deriving Show

data PLMState = PLMState {
  rtStack :: [RTLevel],
  input :: [PortVal],
  output :: [PortVal]
} deriving Show

data PLMVal
  = PLMByte Word8 | PLMWord Word16 | PLMInt Int32 -- for values assigned to variables
  | PLMInteger Integer -- for unspecified number literals
  | PLMBool Bool
  | PLMFun String [Identifier] PLMType BlockBody
  | PLMProc String [Identifier] BlockBody

instance Show PLMVal where
  show (PLMByte x) = show x
  show (PLMWord x) = show x
  show (PLMInt x) = show x
  show (PLMInteger x) = show x
  show (PLMBool b) = show b
  show (PLMFun name _ _ _) = "<fun:" ++ name ++ ">"
  show (PLMProc name _ _) = "<proc:" ++ name ++ ">"



-- Utility functions

levelInsert :: RTLevel -> Identifier -> PLMVal -> RTLevel
levelInsert (RTLevel params (VarMap map)) i v =
  RTLevel params (VarMap ((i, v) : map))

levelGet :: RTLevel -> Identifier -> Maybe PLMVal
levelGet (RTLevel _ (VarMap map)) i = lookup i map

levelSet :: RTLevel -> Identifier -> PLMVal -> RTLevel
levelSet (RTLevel params (VarMap map)) i v =
  RTLevel params (VarMap (mapSet map i v))
  where mapSet [] _ _= []
        mapSet ((k, v0) : t) i v1
          | i == k = (k, v1) : t
          | otherwise = (k, v0) : mapSet t i v

-- add new level and set parameters
newLevel :: PLMState -> [Identifier] -> [PLMVal] -> PLMState
newLevel state names vals =
  let (PLMState rtStack input output) = state
      newParams = VarMap $ zipWith (,) names vals
      newLevel = RTLevel newParams (VarMap [])
  in PLMState (newLevel:rtStack) input output

delLevel :: PLMState -> PLMState
delLevel (PLMState (r:rs) input output) = PLMState rs input output

getLevelParam :: PLMState -> Identifier -> Maybe PLMVal
getLevelParam (PLMState (level:_) _ _) identifier =
  let RTLevel (VarMap params) _ = level
  in lookup identifier params

getDefaultTypeValue :: PLMType -> PLMVal
getDefaultTypeValue TByte = PLMByte 0
getDefaultTypeValue TWord = PLMWord 0
getDefaultTypeValue TInt = PLMInt 0
getDefaultTypeValue TBool = PLMBool False

addIdentifier :: PLMState -> Identifier -> PLMType -> PLMState
addIdentifier state@(PLMState (r:rs) inputList outputList) i t =
  let mParamVal = getLevelParam state i
      val =
        case mParamVal of
          Just v -> v
          Nothing -> getDefaultTypeValue t
  in PLMState ((levelInsert r i val):rs) inputList outputList

addProcedure (PLMState (r:rs) inputList outputList) (Procedure idName params body) =
  let (Identifier name) = idName
      val = PLMProc name params body
  in PLMState ((levelInsert r idName val):rs) inputList outputList

addFunction (PLMState (r:rs) inputList outputList) (Function idName params t b) =
  let (Identifier name) = idName
      val = PLMFun name params t b
  in PLMState ((levelInsert r idName val):rs) inputList outputList

getValue :: PLMState -> Identifier -> Maybe PLMVal
getValue (PLMState (r:rs) _ _) i = levelGet r i


-- for integer variables: convert new value while keeping the type
newVal :: PLMVal -> PLMVal -> PLMVal
newVal (PLMByte byte)  newValue = PLMByte $ fromIntegral (integ newValue)
newVal (PLMWord word)  newValue = PLMWord $ fromIntegral (integ newValue)
newVal (PLMInt int)  newValue = PLMInt $ fromIntegral (integ newValue)
newVal _  newValue = newValue

setValue :: PLMState -> Identifier -> PLMVal -> PLMState
setValue s@(PLMState (r:rs) input output) i v =
  let mv = getValue s i
  in case mv of
    Just ov -> PLMState ((levelInsert r i (newVal ov v)):rs) input output
    Nothing -> error "unreachable code in Eval:setValue"

returnValue = Identifier ""

lookupDelete :: Integer -> [PortVal] -> ([PortVal], Maybe PLMVal)
lookupDelete _ [] = ([], Nothing)
lookupDelete x (PortVal (k, v):t) =
  if x == k
  then (t, Just v)
  else
    let (l1, v1) = lookupDelete x t
    in (PortVal (k,v):l1, v1)

removeInputValue :: PLMState -> PLMVal -> (Maybe PLMVal, PLMState)
removeInputValue (PLMState varList inputList ouputList) (PLMInteger port) =
  let (l, mv) = lookupDelete port inputList
  in (mv, PLMState varList l ouputList)

setOutput :: PLMState -> PLMVal -> PLMVal -> PLMState
setOutput (PLMState vars inList outList) (PLMInteger port) value =
  PLMState vars inList (outList ++ [PortVal (port, value)])


-- evaluation

evalModule :: PLMState -> Module -> PLMState
evalModule state (Module identifier doBlock) = evalDoBlock state  doBlock

evalBlockBody :: PLMState -> BlockBody -> PLMState
evalBlockBody state  (BlockBody dlist blist _) =
  let state1 = foldl (\s d -> evalDeclaration s d) state dlist
  in foldl (\s b -> evalBodyItem s b) state1 blist

evalDeclaration :: PLMState -> Declaration -> PLMState
evalDeclaration state (VariableDeclaration i t) = addIdentifier state i t
evalDeclaration state p@(Procedure _ _ _) = addProcedure state p
evalDeclaration state f@(Function _ _ _ _) = addFunction state f

evalWhileBlock :: PLMState -> WhileBlock -> PLMState
evalWhileBlock state (WhileBlock cond body) = recfun state
  where recfun state =
          let (condVal, state1) = evalExpr state cond
          in case condVal of
            PLMBool True -> recfun (evalBlockBody state1 body)
            PLMBool False -> state1

-- TODO: expand to include step size and direction
evalIterBlock :: PLMState -> IterBlock -> PLMState
evalIterBlock state (IterBlock var initExpr finalExpr body) =
  let (initialVal, state1) = evalExpr state initExpr
      state2 = setValue state1 var initialVal
      (finalVal, state3) = evalExpr state2 finalExpr
  in recfun state3 (integ initialVal) (integ finalVal) 1
  where
    recfun state current final step =
      if current <= final then
        let s1 = evalBlockBody state body
            new = current + step
            s2 = setValue s1 var (PLMInteger new)
        in recfun s2 new final step
      else state

evalCaseBlock :: PLMState -> CaseBlock -> PLMState
evalCaseBlock state (CaseBlock expression statements) =
  let (PLMInt val, state1) = evalExpr state expression
  in if val < 0 || (fromIntegral val) >= (length statements)
        then error "case: bad index"
        else evalStatement state1 (statements !! (fromIntegral val))

evalDoBlock :: PLMState -> DoBlock -> PLMState
evalDoBlock state (DoBlock _ blockBody) = evalBlockBody state blockBody

evalStatement :: PLMState -> Statement -> PLMState
evalStatement state statement =
  case statement of
    StWhile doWhile -> evalWhileBlock state doWhile
    StIter doIter -> evalIterBlock state doIter
    StCase doCase -> evalCaseBlock state doCase
    StDo doBlock -> evalDoBlock state doBlock
    StAsgn assignment -> evalAssignment state assignment
    StIf ifStat -> evalIfStatement state ifStat
    StCall cstat -> evalCallStatement state cstat
    StRet rstat -> evalReturnStatement state rstat
    StGo gstat -> evalGoStatement state gstat
    StEmpty -> state

evalBodyItem :: PLMState -> BodyItem -> PLMState
evalBodyItem state (BodyItem labels ms) =
  case ms of
    Just statement -> evalStatement state statement
    Nothing -> state

evalIfStatement :: PLMState -> IfStatement -> PLMState
evalIfStatement state (IfStatement condExpr stat mstat) =
  let (cond, state1) = evalExpr state condExpr
  in case cond of
    PLMBool True -> evalStatement state1 stat
    PLMBool False ->
      case mstat of
        Just s -> evalStatement state1 s
        Nothing -> state1


evalAssignment :: PLMState -> Assignment -> PLMState
evalAssignment state0 (Assignment xs expr) =
  let (assVal, state1) = evalExpr state0 expr
  in doAss state1 xs assVal
  where doAss st [] _ = st
        doAss st ((ident,[]):t) val = doAss (setValue st ident val) t val
        doAss st ((ident,[expr]):t) val =
          let (paramVal, state2) = evalExpr st expr
          in case getIdentStr ident of
               "output" -> doAss (setOutput state2 paramVal val) t val
               _ -> doAss state2 t val


evalArgs :: PLMState -> [Expression] -> ([PLMVal], PLMState)
evalArgs state args =
  let (s, fp) = foldl ff (state, []) args
      ff = \(s,a) e ->
             let (val, s1) = evalExpr s e
             in (s1, val:a)
  in (fp, s)

evalCallStatement :: PLMState -> CallStatement -> PLMState
evalCallStatement state (CallStatement expr args) =
  let (p@(PLMProc name params body), state1) = evalExpr state expr
      (argVals, state2) = evalArgs state args
  in runProcedure state2 p argVals

evalReturnStatement :: PLMState -> ReturnStatement -> PLMState
evalReturnStatement state (ReturnStatement expr) =
  let (value1, state1) = evalExpr state expr
  in (setValue state1 returnValue value1)

evalGoStatement :: PLMState -> GoStatement -> PLMState
evalGoStatement state (GoStatement label) = undefined  -- TODO: jump to label

evalExpr :: PLMState -> Expression -> (PLMVal, PLMState)
evalExpr state (Expr le) = evalLogExpr state le
evalExpr state (AExpr (EmbeddedAssignment i le)) =
  let (value, state1)  = evalLogExpr state le
      state2 = setValue state1 i value
  in (value, state2)

integ :: PLMVal -> Integer
integ (PLMByte b) = fromIntegral b
integ (PLMWord w) = fromIntegral w
integ (PLMInt x) = fromIntegral x
integ (PLMInteger x) = x

evalNotOp :: LogNotOp -> (PLMVal -> PLMVal)
evalNotOp LogNot (PLMBool x) = PLMBool (not x)
evalNotOp LogNot v = PLMInteger (complement (integ v))

evalOrOp :: LogOrOp -> (PLMVal -> PLMVal -> PLMVal)
evalOrOp OrOp (PLMBool x) (PLMBool y) = PLMBool (x || y)
evalOrOp XorOp (PLMBool x) (PLMBool y) = PLMBool (x /= y)
evalOrOp OrOp x y = PLMInteger ((.|.) (integ x) (integ y))
evalOrOp XorOp x y = PLMInteger (xor (integ x) (integ y))

evalAndOp ::LogAndOp -> (PLMVal -> PLMVal -> PLMVal)
evalAndOp LogAnd (PLMBool x) (PLMBool y) = PLMBool (x && y)
evalAndOp LogAnd x y = PLMInteger (integ x .&. integ y)

evalRelOp :: RelOp  -> (PLMVal -> PLMVal -> PLMVal)
evalRelOp LEqOp x y = PLMBool (integ x <= integ y)
evalRelOp GEqOp x y = PLMBool (integ x >=  integ y)
evalRelOp LowerOp x y = PLMBool (integ x < integ y)
evalRelOp GreaterOp x y = PLMBool (integ x > integ y)
evalRelOp EqOp x y = PLMBool (integ x == integ y)
evalRelOp NEqOp x y = PLMBool (integ x /=  integ y)

evalSignOp :: SignOp -> (PLMVal -> PLMVal)
evalSignOp SPlus x = x
evalSignOp SMinus v = PLMInteger (- (integ v))

evalAddOp :: AddOp -> (PLMVal -> PLMVal -> PLMVal)
evalAddOp APlus x y = PLMInteger (integ x +  integ y)
evalAddOp AMinus x y = PLMInteger (integ x - integ y)

evalMulOp :: MulOp -> (PLMVal -> PLMVal -> PLMVal)
evalMulOp MulOp x y = PLMInteger (integ x * integ y)
evalMulOp ModOp x y = PLMInteger (mod (integ x) (integ y))
evalMulOp DivOp x y = PLMInteger (div (integ x) (integ y))

evalLogExpr :: PLMState -> LogExpr -> (PLMVal, PLMState)
evalLogExpr state (ULogExpr term) = evalLogTerm state term
evalLogExpr state (BLogExpr term orOp expr) =
  let (value1, state1) = evalLogTerm state term
      (value2, state2) = evalLogExpr state1 expr
      result = (evalOrOp orOp) value1 value2
  in (result, state2)

evalLogTerm :: PLMState -> LogTerm -> (PLMVal, PLMState)
evalLogTerm state (ULogTerm factor) = evalLogFactor state factor
evalLogTerm state (BLogTerm factor andOp term) =
  let (value1, state1) = evalLogFactor state factor
      (value2, state2) = (evalLogTerm state1 term)
      result = (evalAndOp andOp) value1 value2
  in (result, state2)

evalLogUFactor :: PLMState -> LogUFactor -> (PLMVal, PLMState)
evalLogUFactor state (ULogFactor ae) = evalArithmExpr state ae
evalLogUFactor state (BLogFactor ae1 relOp ae2) =
  let (value1, state1) = evalArithmExpr state ae1
      (value2, state2) = evalArithmExpr state1 ae2
      result = (evalRelOp relOp) value1 value2
  in (result, state2)

evalLogFactor :: PLMState -> LogFactor -> (PLMVal, PLMState)
evalLogFactor state (LogFactor Nothing uf) = evalLogUFactor state uf
evalLogFactor state (LogFactor (Just n) uf) =
  let (value1, state1) = evalLogUFactor state uf
      result = (evalNotOp n) value1
  in (result, state1)

evalArithmExpr :: PLMState -> ArithmExpr -> (PLMVal, PLMState)
evalArithmExpr state (UArithmExpr term) = evalArithmTerm state term
evalArithmExpr state (BArithmExpr term addOp expr) =
  let (value1, state1) = (evalArithmTerm state term)
      (value2, state2) = (evalArithmExpr state1 expr)
      result = (evalAddOp addOp) value1 value2
  in (result, state2)

evalArithmTerm :: PLMState -> ArithmTerm -> (PLMVal, PLMState)
evalArithmTerm state (UArithmTerm factor) = evalArithmFactor state factor
evalArithmTerm state (BArithmTerm factor mulOp term) =
  let (value1, state1) = evalArithmFactor state factor
      (value2, state2) = evalArithmTerm state1 term
      result = (evalMulOp mulOp) value1 value2
  in (result, state2)

shr val cnt = fromIntegral (shiftR (integ val) (fromIntegral (integ cnt)))
shl val cnt = fromIntegral (shiftL (integ val) (fromIntegral (integ cnt)))
ror (PLMByte b) cnt = fromIntegral (rotateR b (fromIntegral (integ cnt)))
ror (PLMWord w) cnt = fromIntegral (rotateR w (fromIntegral (integ cnt)))
rol (PLMByte b) cnt = fromIntegral (rotateL b (fromIntegral (integ cnt)))
rol (PLMWord w) cnt = fromIntegral (rotateL w (fromIntegral (integ cnt)))

evalArithmUFactor :: PLMState -> ArithmUFactor -> (PLMVal, PLMState)
evalArithmUFactor state (PConst x) = (PLMInteger x, state)
evalArithmUFactor state (PVar name args) =
  case getValue state name of
    Just f@(PLMFun _ _ _ _) ->
      let (ps, state1) = evalArgs state args
      in runFunction state1 f ps
    Just v -> (v, state)
    Nothing -> case getIdentStr name of
        "shr" ->
          let (value, s1) = evalExpr state (args !! 0)
              (count, s2) = evalExpr s1 (args !! 1)
          in (PLMInteger (shr value count), s1)
        "shl" ->
          let (value, s1) = evalExpr state (args !! 0)
              (count, s2) = evalExpr s1 (args !! 1)
          in (PLMInteger (shl value count), s1)
        "ror" ->
          let (value, s1) = evalExpr state (args !! 0)
              (count, s2) = evalExpr s1 (args !! 1)
          in (PLMInteger (ror value count), s1)
        "rol" ->
          let (value, s1) = evalExpr state (args !! 0)
              (count, s2) = evalExpr s1 (args !! 1)
          in (PLMInteger (rol value count), s1)
        "input" ->
          let (port, state1) = evalExpr state  (args !! 0)
              (Just value, state2) = removeInputValue state port
          in case removeInputValue state port of
            (Just value, state2) -> (value, state2)
            (Nothing, _) -> error ("evalArithmUFactor: could not remove port "
                                    ++ (show port) ++ " from state "
                                    ++ (show state))
        _ ->  error $ "bad identifier name: " ++ (show name)
evalArithmUFactor state (PExpr expr) = evalExpr state expr

evalArithmFactor :: PLMState -> ArithmFactor -> (PLMVal, PLMState)
evalArithmFactor state (ArithmFactor Nothing uf) = evalArithmUFactor state uf
evalArithmFactor state (ArithmFactor (Just sign) uf) =
  let (value, state1) = evalArithmUFactor state uf
  in ((evalSignOp sign) value, state1)

runFunction :: PLMState -> PLMVal -> [PLMVal] ->(PLMVal, PLMState)
runFunction state function args =
  let PLMState levels inputList outputList = state
      PLMFun name params returnType body = function
      state1 = newLevel state params args
      state2 = evalBlockBody state1 body
      val = fromJust $ getValue state2 returnValue
      state3 = delLevel state2
  in (val, state3)

runProcedure :: PLMState -> PLMVal -> [PLMVal] -> PLMState
runProcedure state procedure args =
  let PLMState levels inputList outputList = state
      PLMProc name params body = procedure
      state1 = newLevel state params args
      state2 = evalBlockBody state1 body
      state3 = delLevel state2
  in state3

evalString pFun eFun string =
  let pRes = parseString pFun string
  in case pRes of
       Left e -> error (show e)
       Right node -> do
         let state = PLMState [RTLevel (VarMap []) (VarMap [])] [] []
         return $ eFun state node

str2val :: String -> Maybe PLMVal
str2val string =
  case readMaybe string :: Maybe Integer of
    Just x -> Just $ PLMInteger x
    Nothing -> case readMaybe string :: Maybe Bool of
      Just b -> Just $ PLMBool b
      Nothing -> Nothing

runModule :: String -> [PortVal] -> PLMState
runModule string input =
  let state = PLMState [RTLevel (VarMap []) (VarMap []) ] input []
      pRes = parseString pModule string
  in case pRes of
       Left e -> error (show e)
       Right node -> evalModule state node
